package com.kotlinx.view.tipsDialog

import android.app.Activity
import android.content.res.Resources
import android.graphics.Color
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.kotlinx.view.R
import com.kotlinx.view.base.YBaseDialog
import com.kotlinx.view.color.ViewColor
import com.kotlinx.view.databinding.YDialogBinding
import com.kotlinx.view.databinding.YDialogTextviewBinding
import com.kotlinx.view.utils.YDrawableUtils

/**
 * 提示弹窗，可以设置标题、正文、确定按钮、取消按钮，可以设置全部元素颜色
 * @author yujing 2022年3月14日23:00:40
 */
/*
用法：
//"提示-无"
val dialog = YViewTipsDialog(this, "这是一条提示消息").apply {
    cancel = true
    alpha = 0.8f //透明
    dimAmount = 0.4f //模糊
    widthPixels = 0.3f //宽
    heightPixels = 0.3f //高
    viewColor.apply {
        fillBackgroundColor = Color.parseColor("#F7F5C2")
        fillStrokeColor = Color.GRAY
        contentTextColor = Color.parseColor("#ED8A26")
    }
    show(2000)
}


//"提示-确定"
val dialog = YViewTipsDialog(this, "是否确定？", null).apply {
    okListener = {
        dismiss()
        //确定逻辑
    }
    show()
}

//"提示-标题-确定"
val dialog = YViewTipsDialog(this, "是否确定？", "提示").apply {
    cancel = false
    okListener = {
        dismiss()
        //确定逻辑
    }
    show()
}

//"提示-标题-确定-取消"
val dialog = YViewTipsDialog(this, "是否确定？", "提示").apply {
    cancel = false
    okListener = {
        dismiss()
        //确定逻辑
    }
    cancelListener = {
        dismiss()
        //取消取消
    }
    show()
}

//"提示-标题-确定-取消-改颜色"
val dialog = YViewTipsDialog(this, "是否确定？", "提示").apply {
    cancel = false
    btNameOK = "确定"
    btNameCancel = "取消"
    viewColor.apply {
        //标题文字颜色
        titleTextColor = Color.WHITE
        //标题背景颜色
        titleBackgroundColor =  Color.parseColor("#4080FF")
        //标题分割线颜色
        titleDividerColor = Color.parseColor("#604080FF")
        //按钮上面分割线颜色
        contentDividerColor = Color.parseColor("#604080FF")
        //ok 按钮 抬起颜色
        pressedFalseButtonOkColor = Color.parseColor("#4080FF")
        //ok 按钮 按下颜色
        pressedTrueButtonOkColor= Color.parseColor("#604080FF")
        //cancel 按钮 抬起颜色
        pressedFalseButtonCancelColor = Color.parseColor("#4080FF")
        //cancel 按钮 按下颜色
        pressedTrueButtonCancelColor = Color.parseColor("#604080FF")
        //确定按键文字颜色
        buttonOkTextColor = Color.WHITE
        //取消按键文字颜色
        buttonCancelTextColor = Color.WHITE
        //按键间 分割线颜色
        buttonDividerColor= Color.WHITE
    }
    okListener = {
        dismiss()
        //确定逻辑
    }
    cancelListener = {
        dismiss()
        //取消取消
    }
    show()
}


java:
YViewTipsDialog dialog = new YViewTipsDialog(this, "是否确定？", "提示");
dialog.setCancel(true);
dialog.setOkListener(() -> {
    //确定
    return Unit.INSTANCE;
});
dialog.setCancelListener(() -> {
    //取消
    return Unit.INSTANCE;
});
dialog.show();
 */
class YViewTipsDialog(activity: Activity, var content: CharSequence? = null, var titleText: CharSequence? = null) : YBaseDialog<YDialogBinding>(activity, R.layout.y_dialog, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar) {
    //确定按钮
    var okListener: (() -> Unit)? = null

    //取消按钮
    var cancelListener: (() -> Unit)? = null

    var btNameOK: CharSequence = "确　定"
    var btNameCancel: CharSequence = "取　消"

    //文字大小比例
    var viewSizeScale = 1.0f

    //颜色
    var viewColor: ViewColor

    //弹窗中心view
    lateinit var contentBinding: YDialogTextviewBinding

    //初始化完成
    var initCompleteListener: ((YDialogBinding) -> Unit)? = null

    init {
        this.viewColor = ViewColor(activity)
        val width = Resources.getSystem().displayMetrics.widthPixels
        val height = Resources.getSystem().displayMetrics.heightPixels
        //如果是竖屏
        viewSizeScale = if (height > width) 1.25f else 1f
        //初始view
        initContentView()
    }

    override fun initBefore() {
        fillColor = viewColor.fillBackgroundColor //整体填充颜色
        strokeColor = viewColor.fillStrokeColor  //整体边框颜色
    }

    //初始化中心view
    private fun initContentView() {
        contentBinding = DataBindingUtil.bind(View.inflate(activity, R.layout.y_dialog_textview, null))!!
    }

    override fun init() {
        //设置标题
        binding.tvTitle.text = titleText
        binding.llContent.removeAllViews()
        binding.llContent.addView(contentBinding.root)
        contentBinding.tvContent.text = content
        //设置确定按钮
        binding.btOk.text = btNameOK
        binding.btOk.setOnClickListener { v -> okListener?.invoke() }
        //设置取消按钮
        binding.btCancel.text = btNameCancel
        binding.btCancel.setOnClickListener { v -> if (cancelListener == null) dismiss() else cancelListener?.invoke() }

        //---------------------------设置显示 开始---------------------------
        binding.run {
            if (titleText == null) {
                llTitle.visibility = View.GONE
                ivTitleDivider.visibility = View.GONE
            }

            if (okListener == null) {
                btOk.visibility = View.GONE
                ivButtonDivider.visibility = View.GONE
            }

            if (cancelListener == null) {
                btCancel.visibility = View.GONE
                ivButtonDivider.visibility = View.GONE
            }

            if (okListener == null && cancelListener == null) {
                llBt.visibility = View.GONE
                ivContentDivider.visibility = View.GONE
            }
        }
        //---------------------------设置显示 结束---------------------------


        //---------------------------缩放 开始---------------------------
        if (viewSizeScale != 1.0f) {
            binding.run {
                //设置文字放大缩小倍数
                tvTitle.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                contentBinding.tvContent.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                btCancel.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                btOk.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }

                //标题高度
                llTitle.run {
                    layoutParams = (layoutParams as LinearLayout.LayoutParams).apply { height = (height * viewSizeScale).toInt() }
                }

                //按钮高度缩放
                llBt.run {
                    layoutParams = (layoutParams as LinearLayout.LayoutParams).apply { height = (height * viewSizeScale).toInt() }
                }
            }
        }
        //---------------------------缩放 结束---------------------------


        //---------------------------设置颜色 开始---------------------------
        binding.run {
            llTitle.setBackgroundColor(viewColor.titleBackgroundColor)
            tvTitle.setTextColor(viewColor.titleTextColor)
            ivTitleDivider.setBackgroundColor(viewColor.titleDividerColor)

            llContent.setBackgroundColor(viewColor.contentBackgroundColor)
            contentBinding.tvContent.setTextColor(viewColor.contentTextColor)
            ivContentDivider.setBackgroundColor(viewColor.contentDividerColor)

            btCancel.setTextColor(viewColor.buttonCancelTextColor)
            btOk.setTextColor(viewColor.buttonOkTextColor)
            ivButtonDivider.setBackgroundColor(viewColor.buttonDividerColor)

            val focusedDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.focusedTrueButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val pressedDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.pressedTrueButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val normalDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.pressedFalseButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            btOk.background = YDrawableUtils.createStateListDrawable(focusedDrawableOk, pressedDrawableOk, normalDrawableOk)

            val focusedDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.focusedTrueButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val pressedDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.pressedTrueButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val normalDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.pressedFalseButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            btCancel.background = YDrawableUtils.createStateListDrawable(focusedDrawableCancel, pressedDrawableCancel, normalDrawableCancel)
        }
        //---------------------------设置颜色 结束---------------------------

        //初始化完成
        initCompleteListener?.invoke(binding)
    }
}
