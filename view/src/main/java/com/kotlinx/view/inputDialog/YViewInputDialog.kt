package com.kotlinx.view.inputDialog

import android.app.Activity
import android.content.res.Resources
import android.graphics.Color
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.kotlinx.view.R
import com.kotlinx.view.base.YBaseDialog
import com.kotlinx.view.color.ViewColor
import com.kotlinx.view.databinding.YDialogBinding
import com.kotlinx.view.databinding.YDialogEdittextBinding
import com.kotlinx.view.utils.YDrawableUtils

/**
 * 输入弹窗
 * @author yujing 2023年8月17日17:27:37
 */
/*
用法：
//"输入-确定"
val dialog = YViewInputDialog(this, "", "请输入内容").apply {
    hint = "请输入内容"
    okListener = {
        dismiss()
        YToast.show(it)
    }
    show()
}

//"输入-确定-数字"
val dialog = YViewInputDialog(this, "", "请输入内容").apply {
    hint = "请输入"
    tipsUp = "请输入 >=10 的数"
    heightPixels = 0.5f
    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
    okListener = fun(it: String) {
        val v = it.toDoubleOrNull()
        if (v == null || v < 10) return YToast.show("校验不通过")
        dismiss()
        YToast.show(it)
    }
    //限制长度
    initCompleteListener = {
        contentBinding.etContent.filters = arrayOf(InputFilter.LengthFilter(8))
    }
    //校验数据
    contentBinding.etContent.addTextChangedListener {
        if (it.toString().isNotEmpty()) {
            val value = it.toString().toDoubleOrNull()
            contentBinding.tvTipsUp.setTextColor(if (value == null || value < 10) Color.RED else Color.GRAY)
        }
    }
    show()
}

//"输入-确定-取消"
val dialog = YViewInputDialog(this, "默认值", "请输入内容").apply {
    okListener = {
        dismiss()
        YToast.show(it)
    }
    cancelListener = {
        dismiss()
        //取消逻辑
    }
    show()
}

//更改颜色
val dialog = YViewInputDialog(this, "川", "输入车牌号").apply {
    hint = "请输入车牌号："
    viewColor.apply {
        //ok 按钮 抬起颜色
        pressedFalseButtonOkColor = Color.parseColor("#E9A633")
        //确定按键文字颜色
        buttonOkTextColor = Color.WHITE
    }
    okListener = {
        dismiss()
        YToast.show(it)
    }
    show()
}
 */
class YViewInputDialog(activity: Activity, var content: CharSequence? = null, var titleText: CharSequence? = null) : YBaseDialog<YDialogBinding>(activity, R.layout.y_dialog, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar) {
    //确定按钮
    var okListener: ((value: String) -> Unit)? = null

    //取消按钮
    var cancelListener: (() -> Unit)? = null
    var btNameOK: CharSequence = "确　定"
    var btNameCancel: CharSequence = "取　消"

    //输入框提示文字
    var hint = ""
        set(value) {
            field = value
            contentBinding.etContent.hint = hint
        }

    //输入框上方提示文字
    var tipsUp: CharSequence? = null
        set(value) {
            field = value
            contentBinding.tvTipsUp.text = tipsUp
        }

    //输入框下方提示文字
    var tipsBottom: CharSequence? = null
        set(value) {
            field = value
            contentBinding.tvTipsBottom.text = tipsBottom
        }

    //输入框类型
    var inputType = InputType.TYPE_CLASS_TEXT

    //文字或View大小比例
    var viewSizeScale = 1.0f

    //颜色
    var viewColor: ViewColor

    var maxLength = Int.MAX_VALUE //最大输入长度

    //弹窗中心view
    lateinit var contentBinding: YDialogEdittextBinding

    //初始化完成
    var initCompleteListener: ((YDialogBinding) -> Unit)? = null

    init {
        this.viewColor = ViewColor(activity)
        val width = Resources.getSystem().displayMetrics.widthPixels
        val height = Resources.getSystem().displayMetrics.heightPixels
        //如果是竖屏
        viewSizeScale = if (height > width) 1.25f else 1f
        //初始view
        initContentView()
    }

    override fun initBefore() {
        fillColor = viewColor.fillBackgroundColor //整体填充颜色
        strokeColor = viewColor.fillStrokeColor  //整体边框颜色
    }

    //初始化中心view
    private fun initContentView() {
        contentBinding = DataBindingUtil.bind(View.inflate(activity, R.layout.y_dialog_edittext, null))!!
    }

    override fun init() {
        //设置标题
        binding.tvTitle.text = titleText
        binding.llContent.removeAllViews()
        binding.llContent.addView(contentBinding.root)
        contentBinding.tvTipsUp.text = tipsUp
        contentBinding.tvTipsBottom.text = tipsBottom
        contentBinding.etContent.setText(content)
        contentBinding.etContent.hint = hint
        contentBinding.etContent.inputType = inputType
        contentBinding.etContent.run { setSelection(text.toString().length) }
        //设置最大输入长度
        contentBinding.etContent.filters = arrayOf<InputFilter>(LengthFilter(maxLength))
        //设置确定按钮
        binding.btOk.text = btNameOK
        binding.btOk.setOnClickListener {
            okListener?.invoke(contentBinding.etContent.text.toString())
        }
        //设置取消按钮
        binding.btCancel.text = btNameCancel
        binding.btCancel.setOnClickListener { v -> if (cancelListener == null) dismiss() else cancelListener!!.invoke() }

        //---------------------------设置显示 开始---------------------------
        binding.run {
            if (titleText == null) {
                llTitle.visibility = View.GONE
                ivTitleDivider.visibility = View.GONE
            }

            if (okListener == null) {
                btOk.visibility = View.GONE
                ivButtonDivider.visibility = View.GONE
            }

            if (cancelListener == null) {
                btCancel.visibility = View.GONE
                ivButtonDivider.visibility = View.GONE
            }

            if (okListener == null && cancelListener == null) {
                llBt.visibility = View.GONE
                ivContentDivider.visibility = View.GONE
            }
        }
        //---------------------------设置显示 结束---------------------------


        //---------------------------缩放 开始---------------------------
        if (viewSizeScale != 1.0f) {
            binding.run {
                //设置文字放大缩小倍数
                tvTitle.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                btCancel.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                btOk.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }

                contentBinding.tvTipsUp.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                contentBinding.tvTipsBottom.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                contentBinding.etContent.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }

                //标题高度
                llTitle.run {
                    layoutParams = (layoutParams as LinearLayout.LayoutParams).apply { height = (height * viewSizeScale).toInt() }
                }

                //按钮高度缩放
                llBt.run {
                    layoutParams = (layoutParams as LinearLayout.LayoutParams).apply { height = (height * viewSizeScale).toInt() }
                }
            }
        }
        //---------------------------缩放 结束---------------------------


        //---------------------------设置颜色 开始---------------------------
        binding.run {
            llTitle.setBackgroundColor(viewColor.titleBackgroundColor)
            tvTitle.setTextColor(viewColor.titleTextColor)
            ivTitleDivider.setBackgroundColor(viewColor.titleDividerColor)

            llContent.setBackgroundColor(viewColor.contentBackgroundColor)
            contentBinding.etContent.setTextColor(viewColor.contentTextColor)
            contentBinding.etContent.setHintTextColor(viewColor.contentTextHintColor)
            ivContentDivider.setBackgroundColor(viewColor.contentDividerColor)

            btCancel.setTextColor(viewColor.buttonCancelTextColor)
            btOk.setTextColor(viewColor.buttonOkTextColor)
            ivButtonDivider.setBackgroundColor(viewColor.buttonDividerColor)


            val focusedDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.focusedTrueButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val pressedDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.pressedTrueButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val normalDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.pressedFalseButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            btOk.background = YDrawableUtils.createStateListDrawable(focusedDrawableOk, pressedDrawableOk, normalDrawableOk)


            val focusedDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.focusedTrueButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val pressedDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.pressedTrueButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val normalDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.pressedFalseButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            btCancel.background = YDrawableUtils.createStateListDrawable(focusedDrawableCancel, pressedDrawableCancel, normalDrawableCancel)
        }
        //---------------------------设置颜色 结束---------------------------

        //初始化完成
        initCompleteListener?.invoke(binding)
    }
}
