package com.kotlinx.view.customView

import android.app.Activity
import android.content.res.Resources
import android.graphics.Color
import android.view.View
import android.widget.LinearLayout
import com.kotlinx.view.R
import com.kotlinx.view.base.YBaseDialog
import com.kotlinx.view.color.ViewColor
import com.kotlinx.view.databinding.YDialogBinding
import com.kotlinx.view.utils.YDrawableUtils

/**
 * 自定义view弹窗
 * @author yujing 2023年8月17日17:27:37
 */
/*
用法：

val dialog = YViewCustomDialog(this, getView(this), "一个自定义View").apply {
    okListener = {
        dismiss()
    }
    show()
}

fun getView(context: Context): LinearLayout {
    val linearLayout = LinearLayout(context).apply {
        removeAllViews()
        orientation = LinearLayout.VERTICAL //设置纵向布局
        setPadding(dp2px(context, 10f), dp2px(context, 15f), dp2px(context, 10f), dp2px(context, 10f))
        minimumHeight = dp2px(context, 120f) //最小高度
        minimumWidth = dp2px(context, 120f) //最小宽度
    }
    //实例化一个ProgressBar
    val mProgressBar = ProgressBar(context, null, android.R.attr.progressBarStyle).apply {
        layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
            gravity = Gravity.CENTER //设置中心对其
        }
        isIndeterminate = true
    }
    //实例化一个TextView
    val tv = TextView(context).apply {
        layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
            setMargins(0, dp2px(context, 15f), 0, 0)
            gravity = Gravity.CENTER //设置中心
        }
        textSize = 20f
        setTextColor(Color.parseColor("#802035"))
        text = "这是测试数据"
    }
    //实例化第二个TextView
    val tv2 = TextView(context).apply {
        layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
            setMargins(0, dp2px(context, 5f), 0, 0)
            gravity = Gravity.CENTER //设置中心
        }
        textSize = 15f
        setTextColor(Color.parseColor("#70e035"))
        text = "我是余静"
    }
    linearLayout.addView(mProgressBar)
    linearLayout.addView(tv)
    linearLayout.addView(tv2)
    return linearLayout
}
 */
class YViewCustomDialog(activity: Activity, var customView: View? = null, var titleText: CharSequence? = null) : YBaseDialog<YDialogBinding>(activity, R.layout.y_dialog, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar) {
    //确定按钮
    var okListener: (() -> Unit)? = null

    //取消按钮
    var cancelListener: (() -> Unit)? = null
    var btNameOK: CharSequence = "确　定"
    var btNameCancel: CharSequence = "取　消"

    //文字大小比例
    var viewSizeScale: Float

    //颜色
    var viewColor: ViewColor

    //初始化完成
    var initCompleteListener: ((YDialogBinding) -> Unit)? = null

    init {
        this.viewColor = ViewColor(activity)
        val width = Resources.getSystem().displayMetrics.widthPixels
        val height = Resources.getSystem().displayMetrics.heightPixels
        //如果是竖屏
        viewSizeScale = if (height > width) 1.25f else 1f
    }

    override fun initBefore() {
        fillColor = viewColor.fillBackgroundColor //整体填充颜色
        strokeColor = viewColor.fillStrokeColor  //整体边框颜色
    }

    override fun init() {
        //设置标题
        binding.tvTitle.text = titleText
        binding.llContent.removeAllViews()
        binding.llContent.addView(customView)

        //设置确定按钮
        binding.btOk.text = btNameOK
        binding.btOk.setOnClickListener {
            okListener?.invoke()
        }
        //设置取消按钮
        binding.btCancel.text = btNameCancel
        binding.btCancel.setOnClickListener { v -> if (cancelListener == null) dismiss() else cancelListener!!.invoke() }

        //---------------------------设置显示 开始---------------------------
        binding.run {
            if (titleText == null) {
                llTitle.visibility = View.GONE
                ivTitleDivider.visibility = View.GONE
            }

            if (okListener == null) {
                btOk.visibility = View.GONE
                ivButtonDivider.visibility = View.GONE
            }

            if (cancelListener == null) {
                btCancel.visibility = View.GONE
                ivButtonDivider.visibility = View.GONE
            }

            if (okListener == null && cancelListener == null) {
                llBt.visibility = View.GONE
                ivContentDivider.visibility = View.GONE
            }
        }
        //---------------------------设置显示 结束---------------------------


        //---------------------------缩放 开始---------------------------
        if (viewSizeScale != 1.0f) {
            binding.run {
                //设置文字放大缩小倍数
                tvTitle.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                btCancel.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }
                btOk.run { textSize = px2sp(textSize.toInt()) * viewSizeScale }

                //标题高度
                llTitle.run {
                    layoutParams = (layoutParams as LinearLayout.LayoutParams).apply { height = (height * viewSizeScale).toInt() }
                }

                //按钮高度缩放
                llBt.run {
                    layoutParams = (layoutParams as LinearLayout.LayoutParams).apply { height = (height * viewSizeScale).toInt() }
                }
            }
        }
        //---------------------------缩放 结束---------------------------


        //---------------------------设置颜色 开始---------------------------
        binding.run {
            llTitle.setBackgroundColor(viewColor.titleBackgroundColor)
            tvTitle.setTextColor(viewColor.titleTextColor)
            ivTitleDivider.setBackgroundColor(viewColor.titleDividerColor)

            llContent.setBackgroundColor(viewColor.contentBackgroundColor)
            ivContentDivider.setBackgroundColor(viewColor.contentDividerColor)

            btCancel.setTextColor(viewColor.buttonCancelTextColor)
            btOk.setTextColor(viewColor.buttonOkTextColor)
            ivButtonDivider.setBackgroundColor(viewColor.buttonDividerColor)


            val focusedDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.focusedTrueButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val pressedDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.pressedTrueButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val normalDrawableOk = YDrawableUtils.createGradientDrawable(viewColor.pressedFalseButtonOkColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            btOk.background = YDrawableUtils.createStateListDrawable(focusedDrawableOk, pressedDrawableOk, normalDrawableOk)


            val focusedDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.focusedTrueButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val pressedDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.pressedTrueButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            val normalDrawableCancel = YDrawableUtils.createGradientDrawable(viewColor.pressedFalseButtonCancelColor, 0, Color.WHITE, 0F, 0F, 0F, 0F)
            btCancel.background = YDrawableUtils.createStateListDrawable(focusedDrawableCancel, pressedDrawableCancel, normalDrawableCancel)
        }
        //---------------------------设置颜色 结束---------------------------

        //初始化完成
        initCompleteListener?.invoke(binding)
    }
}
