package com.kotlinx.view.utils

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable

/**
 * Drawable相关工具
 * @author yujing 2022年3月15日09:58:34
 */
object YDrawableUtils {

    /**
     * 创建状态Drawable
     *
     * @param focusedDrawable 选中时Drawable
     * @param pressedDrawable 按下时Drawable
     * @param normalDrawable 抬起时Drawable
     * @return
     */
    /* 用法：
        val focusedDrawable = createGradientDrawable(Color.RED, 0, Color.WHITE, 0F, 0F, 0F, 0F)
        val pressedDrawable = createGradientDrawable(Color.RED, 0, Color.WHITE, 0F, 0F, 0F, 0F)
        val normalDrawable = createGradientDrawable(Color.GREEN, 0, Color.WHITE, 0F, 0F, 0F, 0F)
        binding.btOk.background = createStateListDrawable(focusedDrawable, pressedDrawable, normalDrawable)
     */
    fun createStateListDrawable(focusedDrawable: Drawable, pressedDrawable: Drawable, normalDrawable: Drawable): StateListDrawable {
        val stateListDrawable = StateListDrawable()
        stateListDrawable.addState(intArrayOf(android.R.attr.state_focused), focusedDrawable)
        stateListDrawable.addState(intArrayOf(android.R.attr.state_pressed), pressedDrawable)
        stateListDrawable.addState(intArrayOf(-android.R.attr.state_pressed), normalDrawable)
        return stateListDrawable
    }

    /**
     * 创建渐变圆角 Drawable
     *
     * @param fillColor 填充颜色
     * @param width 边框宽度
     * @param strokeColor 边框颜色
     * @param topLeftRadius 圆角弧度
     * @param topRightRadius 圆角弧度
     * @param bottomRightRadius 圆角弧度
     * @param bottomLeftRadius 圆角弧度
     * @return
     */
    fun createGradientDrawable(fillColor: Int = Color.WHITE, width: Int = 0, strokeColor: Int = Color.WHITE, topLeftRadius: Float = 0F, topRightRadius: Float = 0F, bottomRightRadius: Float = 0F, bottomLeftRadius: Float = 0F): GradientDrawable {
        val gradientDrawable = GradientDrawable()
        //gradientDrawable.cornerRadius = radius.toFloat() //圆角
        gradientDrawable.setColor(fillColor)//填充颜色
        gradientDrawable.setStroke(width, strokeColor)//边框宽度和颜色
        //四角圆角
        gradientDrawable.cornerRadii = floatArrayOf(topLeftRadius, topLeftRadius, topRightRadius, topRightRadius, bottomRightRadius, bottomRightRadius, bottomLeftRadius, bottomLeftRadius)
        return gradientDrawable
    }

}