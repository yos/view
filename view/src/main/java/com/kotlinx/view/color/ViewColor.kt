package com.kotlinx.view.color

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import com.kotlinx.view.R


//内部类，设置颜色
class ViewColor(context: Context) {
    //整体填充背景颜色
    var fillBackgroundColor = ContextCompat.getColor(context, R.color.fillBackgroundColor)

    //整体边框颜色
    var fillStrokeColor = ContextCompat.getColor(context, R.color.fillStrokeColor)


    // ok 按钮 选中颜色
    var focusedTrueButtonOkColor = ContextCompat.getColor(context, R.color.focusedTrueButtonOkColor)

    // ok 按钮 抬起颜色
    var pressedFalseButtonOkColor = ContextCompat.getColor(context, R.color.pressedFalseButtonOkColor)

    // ok 按钮 按下颜色
    var pressedTrueButtonOkColor = ContextCompat.getColor(context, R.color.pressedTrueButtonOkColor)


    // cancel 按钮 选中颜色
    var focusedTrueButtonCancelColor = ContextCompat.getColor(context, R.color.focusedTrueButtonCancelColor)

    // cancel 按钮 抬起颜色
    var pressedFalseButtonCancelColor = ContextCompat.getColor(context, R.color.pressedFalseButtonCancelColor)

    // cancel 按钮 按下颜色
    var pressedTrueButtonCancelColor = ContextCompat.getColor(context, R.color.pressedTrueButtonCancelColor)


    //title 背景颜色
    var titleBackgroundColor = ContextCompat.getColor(context, R.color.titleBackgroundColor)

    //title 文字颜色
    var titleTextColor = ContextCompat.getColor(context, R.color.titleTextColor)

    //title 隔断线颜色
    var titleDividerColor = ContextCompat.getColor(context, R.color.titleDividerColor)


    //content 背景颜色
    var contentBackgroundColor = ContextCompat.getColor(context, R.color.contentBackgroundColor)

    //content 文字颜色
    var contentTextColor = ContextCompat.getColor(context, R.color.contentTextColor)

    //content 提示文字颜色
    var contentTextHintColor = ContextCompat.getColor(context, R.color.contentTextHintColor)

    //content 隔断线颜色
    var contentDividerColor = ContextCompat.getColor(context, R.color.contentDividerColor)


    //确定按键文字颜色
    var buttonOkTextColor = ContextCompat.getColor(context, R.color.buttonOkTextColor)

    //取消按键文字颜色
    var buttonCancelTextColor = ContextCompat.getColor(context, R.color.buttonCancelTextColor)

    //按键间 隔断线颜色
    var buttonDividerColor = ContextCompat.getColor(context, R.color.buttonDividerColor)


    //车牌号键盘 提示文字颜色
    var carKeyboardTipsColor = ContextCompat.getColor(context, R.color.carKeyboardTipsColor) //#FFD9D9D9

    //车牌号键盘 省文字颜色
    var carKeyboardProvinceColor = ContextCompat.getColor(context, R.color.carKeyboardProvinceColor) //#FF000000

    //车牌号键盘 号码文字颜色
    var carKeyboardNumberColor = ContextCompat.getColor(context, R.color.carKeyboardNumberColor)//#FF000000

    //车牌号键盘 确定文字颜色
    var carKeyboardConfirmColor = ContextCompat.getColor(context, R.color.carKeyboardConfirmColor)//#FF007457

    //车牌号键盘 按键文字颜色
    var carKeyboardKeysColor = ContextCompat.getColor(context, R.color.carKeyboardKeysColor) //#FF000000

    //车牌号键盘 其他按键（省份）文字颜色
    var carKeyboardKeysOtherColor = ContextCompat.getColor(context, R.color.carKeyboardKeysOtherColor)//#FF007457


    //单选或多选 文字颜色数组,可以多个，会循环使用
//    var itemContentTextColors: ArrayList<Int> = arrayListOf(
//        Color.parseColor("#773355"),
//        Color.parseColor("#337755"),
//        Color.parseColor("#335577"),
//        Color.parseColor("#775533"),
//        Color.parseColor("#557733"),
//        Color.parseColor("#553377")
//    )
    var itemContentTextColors: ArrayList<Int> = arrayListOf(
        ContextCompat.getColor(context, R.color.itemContentTextColors_1),
        ContextCompat.getColor(context, R.color.itemContentTextColors_2)
    )
}