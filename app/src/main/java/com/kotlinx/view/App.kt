package com.kotlinx.view

import android.app.Application
import com.yujing.utils.*
import kotlin.properties.Delegates

class App : Application() {
    //标准单列
    //companion object {
    //    val instance: App by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {App()}
    //}
    //单列
    companion object {
        private var instance: App by Delegates.notNull()
        fun get() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        YUtils.init(this)
        // 初始化Bugly
        //CrashReport.initCrashReport(this, "55ab0ea7ea", YUtils.isDebug(this));
        //弹出全屏
        YShow.setDefaultFullScreen(true)
        //弹图片全屏
        YImageDialog.setDefaultFullScreen(true)
        //日期选择全屏
        YDateDialog.setDefaultFullScreen(true)
        //activity管理
        YActivityUtil.init(this)
        //打开日志保存
        YLog.saveOpen(YPath.getFilePath(this, "log"))
        //保存最近30天日志
        YLog.delDaysAgo(30)
    }
}