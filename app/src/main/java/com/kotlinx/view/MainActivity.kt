package com.kotlinx.view

import android.content.Context
import android.graphics.Color
import android.text.InputFilter
import android.text.InputType
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.text.buildSpannedString
import androidx.core.text.color
import androidx.core.text.scale
import androidx.core.widget.addTextChangedListener
import com.kotlinx.view.base.KBaseActivity
import com.kotlinx.view.carDialog.YViewCarDialog
import com.kotlinx.view.customView.YViewCustomDialog
import com.kotlinx.view.inputDialog.YViewInputDialog
import com.kotlinx.view.selectDialog.YViewSelectDialog
import com.kotlinx.view.test.R
import com.kotlinx.view.test.databinding.ActivityAllTestBinding
import com.kotlinx.view.tipsDialog.YViewTipsDialog
import com.yujing.utils.TTS
import com.yujing.utils.YPermissions
import com.yujing.utils.YRunOnceOfTime
import com.yujing.utils.YScreenUtil.dp2px
import com.yujing.utils.YToast
import com.yujing.utils.YUtils
import com.yutils.view.utils.Create

@Suppress("unused")
class MainActivity : KBaseActivity<ActivityAllTestBinding>(R.layout.activity_all_test) {

    override fun init() {
        YPermissions.requestAll(this)
        binding.wll.removeAllViews()
        binding.ll.removeAllViews()

        Create.button(binding.wll, "退出测试APP") {
            finish()
        }


        Create.button(binding.wll, "提示-无") {
            val dialog = YViewTipsDialog(this, "这是一条提示消息").apply {
                cancel = true
                alpha = 0.8f //透明
                dimAmount = 0.4f //模糊
//                widthPixels = 0.3f //宽
//                heightPixels = 0.3f //高
                viewColor.apply {
                    fillBackgroundColor = Color.parseColor("#F7F5C2")
                    fillStrokeColor = Color.GRAY
                    contentTextColor = Color.parseColor("#ED8A26")
                }
                show(2000)
            }
        }

        Create.button(binding.wll, "提示-确定") {
            val dialog = YViewTipsDialog(this, "是否确定？", null).apply {
                okListener = {
                    dismiss()
                    //确定逻辑
                }
                show()
            }
        }

        Create.button(binding.wll, "提示-标题-确定") {
            val dialog = YViewTipsDialog(this, "是否确定？", "提示").apply {
                cancel = false
                okListener = {
                    dismiss()
                    //确定逻辑
                }
                show()
            }
        }

        Create.button(binding.wll, "提示-确定-取消") {
            val dialog = YViewTipsDialog(this, "是否确定？", "提示").apply {
                cancel = false
                okListener = {
                    dismiss()
                    //确定逻辑
                }
                cancelListener = {
                    dismiss()
                    //取消取消
                }
                show()
            }
        }
        Create.button(binding.wll, "提示-改颜色1") {
            val dialog = YViewTipsDialog(this, "是否确定？", "提示").apply {
                cancel = false
                btNameOK = "确定"
                btNameCancel = "取消"
                viewColor.apply {
                    //ok 按钮 抬起颜色
                    pressedFalseButtonOkColor = Color.parseColor("#E9A633")
                    //cancel 按钮 抬起颜色
                    pressedFalseButtonCancelColor = Color.parseColor("#E9A633")
                    //确定按键文字颜色
                    buttonOkTextColor = Color.WHITE
                    //取消按键文字颜色
                    buttonCancelTextColor = Color.WHITE
                }
                okListener = {
                    dismiss()
                    //确定逻辑
                }
                cancelListener = {
                    dismiss()
                    //取消取消
                }
                show()
            }
        }
        Create.button(binding.wll, "提示-改颜色2") {
            val dialog = YViewTipsDialog(this, "是否确定？", "提示").apply {
                cancel = false
                btNameOK = "确定"
                btNameCancel = "取消"
                viewColor.apply {
                    //标题文字颜色
                    titleTextColor = Color.WHITE
                    //标题背景颜色
                    titleBackgroundColor = Color.parseColor("#4080FF")
                    //标题分割线颜色
                    titleDividerColor = Color.parseColor("#604080FF")
                    //按钮上面分割线颜色
                    contentDividerColor = Color.parseColor("#604080FF")
                    //ok 按钮 抬起颜色
                    pressedFalseButtonOkColor = Color.parseColor("#4080FF")
                    //ok 按钮 按下颜色
                    pressedTrueButtonOkColor = Color.parseColor("#604080FF")
                    //cancel 按钮 抬起颜色
                    pressedFalseButtonCancelColor = Color.parseColor("#4080FF")
                    //cancel 按钮 按下颜色
                    pressedTrueButtonCancelColor = Color.parseColor("#604080FF")
                    //确定按键文字颜色
                    buttonOkTextColor = Color.WHITE
                    //取消按键文字颜色
                    buttonCancelTextColor = Color.WHITE
                    //按键间 分割线颜色
                    buttonDividerColor = Color.WHITE
                }
                okListener = {
                    dismiss()
                    //确定逻辑
                }
                cancelListener = {
                    dismiss()
                    //取消取消
                }
                show()
            }
        }
        Create.space(binding.wll)
        Create.button(binding.wll, "输入-确定") {
            val dialog = YViewInputDialog(this, "", "请输入内容").apply {
                hint = "请输入内容"
                maxLength = 6
                okListener = {
                    dismiss()
                    YToast.show(it)
                }
                show()
            }
        }
        Create.button(binding.wll, "输入-确定-数字") {
            val dialog = YViewInputDialog(this, "", "请输入内容").apply {
                hint = "请输入"
                tipsUp = "请输入 >=10 的数"
                heightPixels = 0.4f
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                okListener = fun(it: String) {
                    val v = it.toDoubleOrNull()
                    if (v == null || v < 10) return YToast.show("校验不通过")
                    dismiss()
                    YToast.show(it)
                }
                //限制长度
                initCompleteListener = {
                    contentBinding.etContent.filters = arrayOf(InputFilter.LengthFilter(8))
                }
                //校验数据
                contentBinding.etContent.addTextChangedListener {
                    if (it.toString().isNotEmpty()) {
                        val value = it.toString().toDoubleOrNull()
                        contentBinding.tvTipsUp.setTextColor(if (value == null || value < 10) Color.RED else Color.GRAY)
                    }
                    tipsBottom = it.toString()
                }
                show()
            }
        }
        Create.button(binding.wll, "输入-确定-取消") {
            val dialog = YViewInputDialog(this, "默认值", "请输入内容").apply {
                okListener = {
                    dismiss()
                    YToast.show(it)
                }
                cancelListener = {
                    dismiss()
                    //取消逻辑
                }
                show()
            }
        }

        Create.space(binding.wll)
        Create.button(binding.wll, "选择-确定-取消") {
            val list = arrayListOf("单项选择————001内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长", "单项选择————002", "单项选择————003", "单项选择————004", "单项选择————005")
            val dialog = YViewSelectDialog(this, list.size, "请选择一个选项").apply {
                cancel = true

                itemName = { position -> list[position] }
                okListener = { position ->
                    if (position != -1) {
                        val item = list[position]
                        YToast.show("选择了: ${item}")
                    }
                    dismiss()
                }
                cancelListener = {
                    dismiss()
                    //取消逻辑
                }
                show()
            }
        }
        Create.button(binding.wll, "选择-点击直接选择") {
            val list = arrayListOf("单项选择————001内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长", "单项选择————002", "单项选择————003", "单项选择————004", "单项选择————005", "单项选择————006", "单项选择————007", "单项选择————008", "单项选择————009")
            val dialog = YViewSelectDialog(this, list.size, "请选择一个选项").apply {
                isSingleSelect = true
                cancel = true
                itemName = { position -> list[position] }
                okListener = { position ->
                    if (position != -1) {
                        val item = list[position]
                        YToast.show("选择了: ${item}")
                    }
                    dismiss()
                }
                show()
            }
        }

        Create.button(binding.wll, "选择-修改颜色1") {
            val list = arrayListOf("单项选择————001内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长", "单项选择————002", "单项选择————003", "单项选择————004", "单项选择————005")
            val dialog = YViewSelectDialog(this, list.size, "请选择一个选项").apply {
                cancel = true
                viewColor.itemContentTextColors = arrayListOf(
                    Color.parseColor("#773355"),
                    Color.parseColor("#337755"),
                    Color.parseColor("#335577"),
                    Color.parseColor("#775533"),
                    Color.parseColor("#557733"),
                    Color.parseColor("#553377")
                )
                itemName = { position -> list[position] }
                okListener = { position ->
                    if (position != -1) {
                        val item = list[position]
                        YToast.show("选择了: ${item}")
                    }
                    dismiss()
                }
                show()
            }
        }
        Create.button(binding.wll, "选择-修改颜色2") {
            val strings = arrayListOf(
                "您好，请查看小程序首页，了解更多详细",
                "单项选择1",
                "单项选择2",
                "单项选择3",
                "单项选择4，非常多内容，非常多内容，非常多内容，非常多内容，非常多内容，非常多内容",
                "单项选择5"
            )
            val colors = arrayListOf("#773355", "#337755", "#FF0000", "#00ff00", "#335577", "#775533", "#557733", "#553377")
            val list = arrayListOf<CharSequence>()
            for (i in strings.indices) {
                list.add(buildSpannedString {
                    scale(0.7F) {
                        color(Color.parseColor(colors[i % colors.size])) {
                            append(strings[i])
                        }
                    }
                })
            }
            val dialog = YViewSelectDialog(this, list.size, "请选择一个选项").apply {
                cancel = true
                itemName = { position -> list[position] }
                okListener = { position ->
                    if (position != -1) {
                        val item = list[position]
                        YToast.show("选择了: ${item}")
                    }
                    dismiss()
                }
                show()
            }
        }

        Create.space(binding.wll)
        Create.button(binding.wll, "多选-确定-取消") {
            val list = arrayListOf("多项选择————001", "多项选择————002内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长内容很长", "多项选择————003", "多项选择————004", "多项选择————005")
            val dialog = YViewSelectDialog(this, list.size, "请选择多个选项").apply {
                cancel = true
                btNameOK = "确定"
                btNameCancel = "取消"
                checkBoxType = 2
                itemName = { position -> list[position] }
                okListener = { _ ->
                    val finishList = arrayListOf("")
                    for (i in 0 until checkList.size) {
                        if (checkList[i].isCheck) finishList.add(list[i])
                    }
                    YToast.show("选择了: ${listToString(finishList)}")
                    dismiss()
                }
                cancelListener = {
                    dismiss()
                }
                show()
            }
        }
        Create.button(binding.wll, "多选-默认选择-确定-取消") {
            val list = arrayListOf("多项选择————001", "多项选择————002", "多项选择————003", "多项选择————004", "多项选择————005")
            val dialog = YViewSelectDialog(this, list.size, "请选择多个选项").apply {
                cancel = true
                btNameOK = "确定"
                btNameCancel = "取消"
                checkBoxType = 2
                itemName = { position -> list[position] }
                defaultLinear = {
                    it == 1 || it == 3
                }
                okListener = { _ ->
                    val finishList = arrayListOf("")
                    for (i in 0 until checkList.size) {
                        if (checkList[i].isCheck) finishList.add(list[i])
                    }
                    YToast.show("选择了: ${listToString(finishList)}")
                    dismiss()
                }
                cancelListener = {
                    dismiss()
                }
                show()
            }
        }
        Create.space(binding.wll)
        Create.button(binding.wll, "自定义-确定") {
            fun getView(context: Context): LinearLayout {
                val linearLayout = LinearLayout(context).apply {
                    removeAllViews()
                    orientation = LinearLayout.VERTICAL //设置纵向布局
                    setPadding(dp2px(10f), dp2px(15f), dp2px(10f), dp2px(10f))
                    minimumHeight = dp2px(120f) //最小高度
                    minimumWidth = dp2px(120f) //最小宽度
                }
                //实例化一个ProgressBar
                val mProgressBar = ProgressBar(context, null, android.R.attr.progressBarStyle).apply {
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
                        gravity = Gravity.CENTER //设置中心对其
                    }
                    isIndeterminate = true
                }
                //实例化一个TextView
                val tv = TextView(context).apply {
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
                        setMargins(0, dp2px(15f), 0, 0)
                        gravity = Gravity.CENTER //设置中心
                    }
                    textSize = 20f
                    setTextColor(Color.parseColor("#802035"))
                    text = "这是测试数据"
                }
                //实例化第二个TextView
                val tv2 = TextView(context).apply {
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
                        setMargins(0, dp2px(5f), 0, 0)
                        gravity = Gravity.CENTER //设置中心
                    }
                    textSize = 15f
                    setTextColor(Color.parseColor("#70e035"))
                    text = "我是余静"
                }
                linearLayout.addView(mProgressBar)
                linearLayout.addView(tv)
                linearLayout.addView(tv2)
                return linearLayout
            }

            val dialog = YViewCustomDialog(this, getView(this), "一个自定义View").apply {
                okListener = {
                    dismiss()
                }
                show()
            }
        }

        Create.button(binding.wll, "车牌号") {
            YRunOnceOfTime.run(1000, "按钮防抖") {
                val dialog = YViewCarDialog(this).apply {
                    default = "京"
                    okListener = {
                        YToast.show(it)
                        TTS.speak(it)
                        dismiss()
                    }
                    show()
                }
            }
        }
        Create.button(binding.wll, "车牌号-修改颜色") {
            YRunOnceOfTime.run(1000, "按钮防抖") {
                val dialog = YViewCarDialog(this).apply {
                    default = "川"
                    okListener = {
                        YToast.show(it)
                        TTS.speak(it)
                        dismiss()
                    }
                    viewColor.apply {
                        //车牌号键盘 提示文字颜色
                        carKeyboardTipsColor = Color.parseColor("#888888")
                        //车牌号键盘 省文字颜色
                        carKeyboardProvinceColor = Color.parseColor("#FF5555")
                        //车牌号键盘 号码文字颜色
                        carKeyboardNumberColor = Color.parseColor("#00FF00")
                        //车牌号键盘 确定文字颜色
                        carKeyboardConfirmColor = Color.parseColor("#0EFAAE")
                        //车牌号键盘 按键文字颜色
                        carKeyboardKeysColor = Color.parseColor("#0E8ADE")
                        //车牌号键盘 其他按键（省份）文字颜色
                        carKeyboardKeysOtherColor = Color.parseColor("#EE3ADE")
                    }
                    show()
                }
            }
        }
    }

    private fun listToString(a: List<Any>?): String {
        if (a == null) return "null"
        val iMax = a.size - 1
        if (iMax == -1) return "[]"
        val b = StringBuilder()
        b.append('[')
        var i = 0
        while (true) {
            b.append(a[i].toString())
            if (i == iMax) return b.append(']').toString()
            if (i != 0) b.append(", ")
            i++
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        YUtils.exit()
    }
}
