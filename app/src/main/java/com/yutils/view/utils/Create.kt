package com.yutils.view.utils

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.yujing.utils.YApp
import com.yujing.utils.YScreenUtil.dp2px
import com.yujing.view.YWarpLinearLayout
import kotlin.random.Random

/**
 * 动态创建view
 */
/*
//文本框
val textView1 = Create.textView(binding.ll)

//按钮
Create.button(binding.wll, "按钮") {}

//输入框
val editText = Create.editText(binding.wll, "输入")

//复选框
Create.checkBox(binding.wll, "选中", true).setOnCheckedChangeListener { _, isCheck ->
   //isCheck
}

//选择开关
Create.switch(binding.wll, "选中", true).setOnCheckedChangeListener { _, isCheck ->
   //isCheck
}

//下拉列表
val array = arrayOf("水星", "金星", "地球", "火星", "木星", "土星", "天王星", "海王星")
Create.spinner(binding.wll, array, 0) {
    YToast.show("你选择了：" + array[it])
}

//垂直布局
val lv = Create.linearLayoutV(binding.wll) //linearLayoutVInScrollView

//水平布局
val lh = Create.linearLayoutH(binding.wll) //linearLayoutHInHorizontalScrollView

 */
@Suppress("MemberVisibilityCanBePrivate")
class Create {
    companion object {
        //获取随机颜色0-255，Random.nextInt(min, max)包含min，不包含max，透明度0-255，最低浓度，最高浓度
        fun randomColor(alpha: Int = 255, min: Int = 0, max: Int = 256) = Color.argb(alpha, Random.nextInt(min, max), Random.nextInt(min, max), Random.nextInt(min, max))

        //动态创建TextView
        fun textView(value: String? = null, color: Int = Color.parseColor("#FF102030")): TextView {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.apply {
                marginStart = dp2px(5f)
                marginEnd = dp2px(5f)
                topMargin = dp2px(5f)
                bottomMargin = dp2px(5f)
            }
            val view = TextView(YApp.get()).apply {
                value?.let { text = it }
                setTextColor(color)
                //height = dp2px(30F)
                gravity = Gravity.CENTER
                setTextIsSelectable(true)
                this.layoutParams = layoutParams
            }
            return view
        }

        //动态创建TextView
        fun textView(linearLayout: ViewGroup?, value: String? = null, color: Int = Color.parseColor("#FF102030")): TextView {
            val view = textView(value, color)
            linearLayout?.addView(view)
            return view
        }

        //动态创建Button,之前颜色：Color.parseColor("#FF169CFA")
        fun button(name: String? = "按钮", color: Int = randomColor(255, 50, 256), listener: View.OnClickListener): Button {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            val view = Button(YApp.get()).apply {
                isAllCaps = false//不自动变大写
                name?.let { text = it }
                this.layoutParams = layoutParams
                setOnClickListener(listener)
                setTextColor(Color.parseColor("#FFFFFFFF"))
                backgroundTintList = ColorStateList(arrayOf(IntArray(0)), intArrayOf(color))
            }
            return view
        }

        //动态创建Button,之前颜色：Color.parseColor("#FF169CFA")
        fun button(linearLayout: ViewGroup?, name: String = "按钮", color: Int = randomColor(255, 50, 256), listener: View.OnClickListener): Button {
            val view = button(name, color, listener)
            linearLayout?.addView(view)
            return view
        }

        //动态创建EditText
        fun editText(value: String = "", hint: String = "在这儿输入内容"): EditText {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.apply {
                marginStart = dp2px(5f)
                marginEnd = dp2px(5f)
                topMargin = dp2px(5f)
                bottomMargin = dp2px(5f)
            }
            val view = EditText(YApp.get()).apply {
                setText(value)
                this.hint = hint
                this.layoutParams = layoutParams
            }
            return view
        }

        //动态创建EditText
        fun editText(linearLayout: ViewGroup?, value: String = "", hint: String = "在这儿输入内容"): EditText {
            val view = editText(value, hint)
            linearLayout?.addView(view)
            return view
        }

        //动态创建imageView
        fun imageView(bitmap: Bitmap? = null, widthDp: Float = 300f, heightDp: Float = 300f): ImageView {
            val layoutParams = LinearLayout.LayoutParams(dp2px(widthDp), dp2px(heightDp))
            layoutParams.gravity = Gravity.CENTER_HORIZONTAL
            val view = ImageView(YApp.get())
            view.layoutParams = layoutParams
            bitmap?.let { view.setImageBitmap(it) }
            return view
        }

        //动态创建imageView
        fun imageView(linearLayout: ViewGroup?, bitmap: Bitmap? = null, widthDp: Float = 300f, heightDp: Float = 300f): ImageView {
            val view = imageView(bitmap, widthDp, heightDp)
            linearLayout?.addView(view)
            return view
        }

        //动态创建checkBox
        fun checkBox(name: String = "按钮", isChecked: Boolean = false, listener: CompoundButton.OnCheckedChangeListener? = null): CheckBox {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.apply {
                marginStart = dp2px(5f)
                marginEnd = dp2px(5f)
                topMargin = dp2px(5f)
                bottomMargin = dp2px(5f)
            }
            val view = CheckBox(YApp.get()).apply {
                text = name
                this.isChecked = isChecked
                gravity = Gravity.CENTER
                setOnCheckedChangeListener(listener)
                this.layoutParams = layoutParams
                setTextColor(randomColor(255, 0, 200))
            }
            return view
        }

        //动态创建checkBox
        fun checkBox(linearLayout: ViewGroup?, name: String = "按钮", isChecked: Boolean = false, listener: CompoundButton.OnCheckedChangeListener? = null): CheckBox {
            val view = checkBox(name, isChecked, listener)
            linearLayout?.addView(view)
            return view
        }

        //动态创建switch
        fun switch(name: String = "选择", isChecked: Boolean = false, listener: CompoundButton.OnCheckedChangeListener? = null): Switch {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.apply {
                marginStart = dp2px(5f)
                marginEnd = dp2px(5f)
                topMargin = dp2px(5f)
                bottomMargin = dp2px(5f)
            }
            val view = Switch(YApp.get()).apply {
                text = name
                this.isChecked = isChecked
                gravity = Gravity.CENTER
                setOnCheckedChangeListener(listener)
                this.layoutParams = layoutParams
                setTextColor(randomColor(255, 0, 200))
            }
            return view
        }

        //动态创建switch
        fun switch(linearLayout: ViewGroup?, name: String = "选择", isChecked: Boolean = false, listener: CompoundButton.OnCheckedChangeListener? = null): Switch {
            val view = switch(name, isChecked, listener)
            linearLayout?.addView(view)
            return view
        }

        //动态创建linearLayout纵向布局
        fun linearLayoutV(color: Int = randomColor(16), radius: Float = 10f): LinearLayout {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1F)
            layoutParams.apply {
                marginStart = dp2px(5f)
                marginEnd = dp2px(5f)
                topMargin = dp2px(0f)
                bottomMargin = dp2px(0f)
            }

            //圆角
            val gradientDrawable = GradientDrawable().apply {
                setColor(color)//填充颜色
                setStroke(1, color)//边框宽度和颜色
                //四角圆角//cornerRadius = radius.toFloat() //圆角
                cornerRadii = floatArrayOf(radius, radius, radius, radius, radius, radius, radius, radius)
            }

            val view = LinearLayout(YApp.get())
            view.apply {
                removeAllViews()
                this.layoutParams = layoutParams
                orientation = LinearLayout.VERTICAL //设置纵向布局
                setPadding(dp2px(5f), dp2px(0f), dp2px(5f), dp2px(0f))
                minimumHeight = dp2px(0f) //最小高度
                minimumWidth = dp2px(0f) //最小宽度
                weightSum = 1F
                background = gradientDrawable
            }
            return view
        }

        //动态创建linearLayout纵向布局
        fun linearLayoutV(linearLayout: ViewGroup?, color: Int = randomColor(16), radius: Float = 10f): LinearLayout {
            val view = linearLayoutV(color, radius)
            linearLayout?.addView(view)
            return view
        }

        //动态创建linearLayout横向布局
        fun linearLayoutH(color: Int = randomColor(16), radius: Float = 10f): LinearLayout {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1F)
            layoutParams.apply {
                marginStart = dp2px(5f)
                marginEnd = dp2px(5f)
                topMargin = dp2px(5f)
                bottomMargin = dp2px(5f)
            }

            //圆角
            val gradientDrawable = GradientDrawable().apply {
                setColor(color)//填充颜色
                setStroke(1, color)//边框宽度和颜色
                //四角圆角//cornerRadius = radius.toFloat() //圆角
                cornerRadii = floatArrayOf(radius, radius, radius, radius, radius, radius, radius, radius)
            }

            val view = LinearLayout(YApp.get())
            view.apply {
                removeAllViews()
                this.layoutParams = layoutParams
                orientation = LinearLayout.HORIZONTAL //设置纵向布局
                setPadding(dp2px(5f), dp2px(5f), dp2px(5f), dp2px(5f))
                minimumHeight = dp2px(0f) //最小高度
                minimumWidth = dp2px(0f) //最小宽度
                weightSum = 1F
                background = gradientDrawable
            }
            return view
        }

        //动态创建linearLayout横向布局
        fun linearLayoutH(linearLayout: ViewGroup?, color: Int = randomColor(16), radius: Float = 10f): LinearLayout {
            val view = linearLayoutH(color, radius)
            linearLayout?.addView(view)
            return view
        }

        //动态创建linearLayout横向布局
        fun linearLayoutVInScrollView(linearLayout: ViewGroup?, color: Int = randomColor(16)): LinearLayout {
            val view = linearLayoutV(color)
            val scrollView = ScrollView(YApp.get()).apply {
                addView(view)
            }
            linearLayout?.addView(scrollView)
            return view
        }

        //动态创建linearLayout横向布局
        fun linearLayoutHInHorizontalScrollView(linearLayout: ViewGroup?, color: Int = randomColor(16)): LinearLayout {
            val view = linearLayoutH(color)
            val scrollView = HorizontalScrollView(YApp.get()).apply {
                addView(view)
            }
            linearLayout?.addView(scrollView)
            return view
        }

        //动态创建瀑布流布局
        fun yWarpLinearLayout(color: Int = randomColor(16)): YWarpLinearLayout {
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1F)
            layoutParams.apply {
                marginStart = dp2px(5f)
                marginEnd = dp2px(5f)
                topMargin = dp2px(5f)
                bottomMargin = dp2px(5f)
            }
            val view = YWarpLinearLayout(YApp.get())
            view.apply {
                removeAllViews()
                this.layoutParams = layoutParams
                setPadding(dp2px(5f), dp2px(5f), dp2px(5f), dp2px(5f))
                minimumHeight = dp2px(0f) //最小高度
                minimumWidth = dp2px(0f) //最小宽度
                setBackgroundColor(color)
            }
            return view
        }

        //动态创建瀑布流布局
        fun yWarpLinearLayout(linearLayout: ViewGroup?, color: Int = randomColor(16)): YWarpLinearLayout {
            val view = yWarpLinearLayout(color)
            linearLayout?.addView(view)
            return view
        }


        //动态创建下拉框
        fun spinner(list: Array<String>, default: Int = 0, listener: (Int) -> Unit): Spinner {
            val spinner = Spinner(YApp.get())
            //声明一个下拉列表的数组适配器// 第一个参数：上下文，第二个参数：条目布局，第三个参数：要显示的数据
            val adapter: ArrayAdapter<String> = ArrayAdapter(YApp.get(), android.R.layout.simple_list_item_1, list)
            //将适配器给Spinner
            spinner.adapter = adapter
            //设置下拉框默认显示第一项,true禁止OnItemSelectedListener默认自动调用一次
            spinner.setSelection(default, true)
            //给下拉框设置选择监听器，一旦用户选中某一项，就触发监听器的onItemSelected方法
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    listener.invoke(position)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
            return spinner
        }

        //动态创建下拉框
        fun spinner(linearLayout: ViewGroup?, list: Array<String>, default: Int = 0, color: Int = randomColor(16), radius: Float = 10f, listener: (Int) -> Unit): Spinner {
            val ll = linearLayoutV(color = color, radius = radius)
            val spinner = spinner(list, default, listener)
            ll.addView(spinner)
            linearLayout?.addView(ll)
            return spinner
        }

        //添加空行
        fun space(linearLayout: ViewGroup?): Space {
            val view = Space(linearLayout?.context)
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            view.layoutParams = layoutParams
            linearLayout?.addView(view)
            return view
        }
    }
}