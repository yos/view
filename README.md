# view

#### 介绍

提示弹窗，可以设置标题、正文、确定按钮、取消按钮，可以设置全部元素颜色。

支持横屏竖屏，全屏。

## 引用

### [子module添加依赖，当前最新版：————> 1.1.1　　　　![最新版](https://img.shields.io/badge/%E6%9C%80%E6%96%B0%E7%89%88-1.1.1-green.svg)](https://search.maven.org/artifact/com.kotlinx/view)

```
dependencies {
    //更新地址  https://gitee.com/yos/view 建议过几天访问看下有没有新版本
    implementation 'com.kotlinx:view:1.1.1'
}
```

注：如果引用失败，或者工程未引入mavenCentral，请引入：mavenCentral()

```
allprojects {
    repositories {
        mavenCentral()
        //阿里云等...
    }
}
```

### 看图

|                          默认                           |                         指定颜色                          |
|:-----------------------------------------------------:|:-----------------------------------------------------:|
| ![Screenshot_车牌号键盘01](app/doc/Screenshot_车牌号键盘01.png) | ![Screenshot_车牌号键盘02](app/doc/Screenshot_车牌号键盘02.png) |

**车牌号键盘**
可以自定义所有文字颜色，大小。  
可以自定义省份顺序。

```kotlin
YViewCarDialog(this).apply {
    default = "京"
    okListener = {
        YToast.show(it)
        TTS.speak(it)
        dismiss()
    }
    show()
}
```

**车牌号键盘 修改颜色**

```kotlin
YViewCarDialog(this).apply {
    default = "川A"
    okListener = {
        YToast.show(it)
        TTS.speak(it)
        dismiss()
    }
    viewColor.apply {
        //车牌号键盘 提示文字颜色
        carKeyboardTipsColor = Color.parseColor("#888888")
        //车牌号键盘 省文字颜色
        carKeyboardProvinceColor = Color.parseColor("#FF5555")
        //车牌号键盘 号码文字颜色
        carKeyboardNumberColor = Color.parseColor("#00FF00")
        //车牌号键盘 确定文字颜色
        carKeyboardConfirmColor = Color.parseColor("#0EFAAE")
        //车牌号键盘 按键文字颜色
        carKeyboardKeysColor = Color.parseColor("#0E8ADE")
        //车牌号键盘 其他按键（省份）文字颜色
        carKeyboardKeysOtherColor = Color.parseColor("#EE3ADE")
    }
    show()
}
```

### 一般弹窗

|                         提示                         |                         多选                         |
|:--------------------------------------------------:|:--------------------------------------------------:|
| ![Screenshot_车牌号键盘01](app/doc/Screenshot_提示05.png) | ![Screenshot_车牌号键盘02](app/doc/Screenshot_选择02.png) |

**提示**

```kotlin
YViewTipsDialog(this, "是否确定？", "提示").apply {
    cancel = false
    setOnClickListener {
        dismiss()
        //确定逻辑
    }
    setCancelOnClickListener {
        dismiss()
        //取消取消
    }
    show()
}
```

**多选**

```kotlin
val list = arrayListOf("多项选择————001", "多项选择————002", "多项选择————003", "多项选择————004", "多项选择————005")
YViewSelectDialog(this, list.size, "请选择多个选项").apply {
    cancel = true
    btNameOK = "确定"
    btNameCancel = "取消"
    checkBoxType = 2
    itemName = { position -> list[position] }
    okListener = { _ ->
        val finishList = arrayListOf("")
        for (i in 0 until checkList.size) {
            if (checkList[i].isCheck) finishList.add(list[i])
        }
        YToast.show("选择了: ${listToString(finishList)}")
        dismiss()
    }
    cancelListener = {
        dismiss()
    }
    show()
}
```

### 更多功能请查看demo示例
